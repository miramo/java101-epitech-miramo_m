package eu.epitech.java.application;

import com.google.common.collect.Range;

import eu.epitech.java.EvenNumberSelector;
import eu.epitech.java.IntegerPrinter;
import eu.epitech.java.NumberPrinter;

/**
 * Main class that will be used by our application
 * 
 * @author nicolas
 * 
 */
public class App {
	public static void main(String[] args) {
		final int begin = 0;
		final int end = 100;
		System.out.println(String.format(
				"a list of even integer between %d and %d", begin, end));
		NumberPrinter<Integer> printer = new IntegerPrinter();
		printer.doPrint(Range.closed(begin, end), new EvenNumberSelector());

	}
}
